## Basic Authentication in Node.js & AngularJS App
This is a practice app during the NUS Full Stack Foundation course.

## How to get it up and running
* `bower install`
* `npm install`
* Start server with `node server/app.js` or `nodemon`
* Visit the localhost port indicated on your server.

```
Basic Layout Sample

├── README.md
├── bower.json
├── client
│   ├── app
│   │   ├── app.js
│   │   ├── feature
│   │   │   └── feature.controller.js
│   │   └── services
│   │       └── help.service.js
│   ├── assets
│   │   ├── css
│   │   ├── images
│   │   └── messages
│   │       ├── 404.html
│   │       └── 500.html
│   └── index.html
├── package.json
└── server
    └── app.js
```
